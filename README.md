## The Mongodb database consists of two collections:
* Products 
* Orders

Data structure in the "Products" collection:
----

```json
{
	"Name": "Pro ASP.NET Core MVC 2",
	"Category": "Book",
	"Quantity": 16,
	"Price": 37.28,
	"Publisher": {
		"Name": "Apress",
		"Address": "233 Spring St, New York, NY 10013"
	}

}
```

Data structure in the "Orders" collection:
----

```json
{
	"_id": {
		"$oid": "5be426f059e0052abc750282"
	},
	"ShoppingDetails": {
		"FirstName": "Artem",
		"LastName": "Shazhko",
		"Address": "Lozovaya 4, 24a, 29",
		"PhoneNumber": "0776836415"
	},
	"SoldProducts": [{
			"_id": "5be424a247a3c2ad11e10113",
			"Name": "Pro C# 7: With .NET and .NET Core",
			"Quantity": 1,
			"Price": "45.55"
		}, {
			"_id": "5be424a247a3c2ad11e1010b",
			"Name": "Pro ASP.NET Core MVC 2",
			"Quantity": 1,
			"Price": "37.28"
		}
	]
}
```

## Project Setup
1. Install Mongodb
2. Install Redis.
3. In the configuration file of the project "book-shop.api" set the connection string to the Mogodb database.
4. Start "book-shop.api".
5. In the configuration file of the project "book-shop.WebUI" set the connection string to the book-shop.api.
6. In the configuration file of the project "book-shop.WebUI" set the connection string to the Redis
7. Start "book-shop.WebUI"
