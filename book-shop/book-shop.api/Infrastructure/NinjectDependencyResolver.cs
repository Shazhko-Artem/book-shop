﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Mongodb;
using Ninject;
using Ninject.Web.Common;

namespace book_shop.api.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }


        private void AddBindings()
        {
            _kernel.Bind<IMongodbContext>().To<MongodbContext>().InRequestScope();
            _kernel.Bind<IRepository<ProductDefinition>>()
                .To<Models.Databases.Mongodb.MongodbProductDefinitionRepository>();
            _kernel.Bind<IRepository<Order>>()
                .To<Models.Databases.Mongodb.MongodbOrderRepository>();
        }
    }
}