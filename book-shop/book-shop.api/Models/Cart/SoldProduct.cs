﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.api.Models.Cart
{
    /// <summary>
    /// Description sold product
    /// </summary>
    public class SoldProduct : IEquatable<SoldProduct>
    {
        /// <summary>
        /// The product id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The product name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The quantity sold product
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// The price sold product
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Initializes the SoldProduct
        /// </summary>
        public SoldProduct() { }
        /// <summary>
        /// Initializes the SoldProduct
        /// </summary>
        /// <param name="productId">Product id</param>
        /// <param name="name">Product name</param>
        /// <param name="quantity">Product quantity</param>
        public SoldProduct(string productId, string name, int quantity)
        {
            Id = productId;
            Name = name;
            Quantity = quantity;
        }

        public bool Equals(SoldProduct other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id) && string.Equals(Name, other.Name) && Quantity == other.Quantity;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SoldProduct)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Quantity;
                return hashCode;
            }
        }
    }
}