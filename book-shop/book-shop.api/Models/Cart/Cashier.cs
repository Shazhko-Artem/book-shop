﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Commands;

namespace book_shop.api.Models.Cart
{
    /// <summary>
    /// Class for accept order
    /// </summary>
    public class Cashier : ICashier
    {
        private readonly IRepository<Order> _ordersRepository;
        private readonly IRepository<ProductDefinition> _productsRepository;
        /// <summary>
        /// Initializes the Cashier
        /// </summary>
        /// <param name="ordersRepository">Orders repository</param>
        /// <param name="productsRepository">Products repository</param>
        public Cashier(IRepository<Order> ordersRepository, IRepository<ProductDefinition> productsRepository)
        {
            _ordersRepository = ordersRepository;
            _productsRepository = productsRepository;
        }
        /// <summary>
        /// Accept order
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Accepted order</returns>
        public async Task<Order> CheckoutAsync(Order order)
        {
            if (order.SoldProducts == null || order.SoldProducts.Count == 0)
                throw new ArgumentException("The sold products list is empty.");
            if (order.ShoppingDetails == null)
                throw new ArgumentException("Shopping details is null");
            Databases.Commands.DecrementProductQuantityCommandVisitor visitor=new DecrementProductQuantityCommandVisitor(order.SoldProducts);
            
            await _productsRepository.AcceptAsync(visitor);

            return await _ordersRepository.AddAsync(order);
        }
    }
}