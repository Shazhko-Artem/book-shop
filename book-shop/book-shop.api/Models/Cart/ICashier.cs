﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.api.Models.Cart
{
    /// <summary>
    /// Basic interface for accept order
    /// </summary>
    public interface ICashier
    {
        /// <summary>
        /// Accept order
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Accepted order</returns>
        Task<Order> CheckoutAsync(Order order);
    }
}
