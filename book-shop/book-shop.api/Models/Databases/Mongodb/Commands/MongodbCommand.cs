﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Threading.Tasks;
using System.Web;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb.Commands
{
    /// <summary>
    /// Abstract class with basic operations for command to mongodb repository
    /// </summary>
    public abstract class MongodbCommand<T>:Models.Databases.Commands.IRepositoryCommand
    {
        private readonly IMongodbRepository<T> _repository;
        /// <summary>
        /// Initializes the MongodbCommand
        /// </summary>
        /// <param name="repository">The Mongodb repository</param>
        protected MongodbCommand(IMongodbRepository<T> repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Run command
        /// </summary>
        public virtual async Task ExecuteAsync()
        {
            using (var session = await _repository.CreateSessionAsync())
            {
                await RunTransactionWithRetryAsync(session);
            }
        }

        /// <summary>
        /// Run transaction with retry if transaction was corrupted
        /// </summary>
        /// <param name="session">The Mongodb session</param>
        protected virtual async Task RunTransactionWithRetryAsync(IClientSessionHandle session)
        {
            while (true)
            {
                try
                {
                    await RunCommandAsync(session);
                    break;
                }
                catch (MongoException exception)
                {
                    // if transient error, retry the whole transaction
                    if (exception.HasErrorLabel("TransientTransactionError"))
                        continue;
                    else
                        throw;
                }
            }
        }
        /// <summary>
        /// Run transaction and abort session if transaction was corrupted
        /// </summary>
        /// <param name="session">The Mongodb session</param>
        protected virtual async Task RunTransactionAsync(IClientSessionHandle session)
        {

            session.StartTransaction(new TransactionOptions(
                readConcern: ReadConcern.Snapshot,
                writeConcern: WriteConcern.WMajority));

            try
            {
                await RunCommandAsync(session);
            }
            catch
            {
                session.AbortTransaction();
                throw;
            }

            CommitWithRetry(session);
        }
        /// <summary>
        /// Commit with retry if commit was corrupted
        /// </summary>
        /// <param name="session">The Mongodb session</param>
        protected virtual void CommitWithRetry(IClientSessionHandle session)
        {
            while (true)
            {
                try
                {
                    session.CommitTransaction();
                    break;
                }
                catch (MongoException exception)
                {
                    // can retry commit
                    if (exception.HasErrorLabel("UnknownTransactionCommitResult"))
                        continue;
                    else
                        throw;
                }
            }
        }

        /// <summary>
        /// The command will run in transaction
        /// </summary>
        /// <param name="session">The Mongodb session</param>
        protected abstract Task RunCommandAsync(IClientSessionHandle session);
    }
}