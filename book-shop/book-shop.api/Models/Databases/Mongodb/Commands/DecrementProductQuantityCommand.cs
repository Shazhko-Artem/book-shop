﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Cart;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb.Commands
{
    /// <summary>
    /// Command for decrement the quantity products in mongodb repository
    /// </summary>
    public class DecrementProductQuantityCommand : Databases.Mongodb.Commands.MongodbCommand<ProductDefinition>
    {
        private readonly ICollection<SoldProduct> _soldProducts;
        private readonly IMongodbRepository<ProductDefinition> _repository;

        /// <summary>
        /// Initializes the DecrementProductQuantityCommand
        /// </summary>
        /// <param name="soldProducts">Products for sold</param>
        /// <param name="repository">Mongodb repository</param>
        public DecrementProductQuantityCommand(ICollection<SoldProduct> soldProducts, IMongodbRepository<ProductDefinition> repository):base(repository)
        {
            _soldProducts = soldProducts;
            _repository = repository;
        }
        /// <summary>
        /// The command will run in transaction
        /// </summary>
        /// <param name="session">The Mongodb session</param>
        protected override async Task RunCommandAsync(IClientSessionHandle session)
        {
            var filterBuilder = new FilterDefinitionBuilder<ProductDefinition>();
            List<ProductDefinitionMessage> errors = new List<ProductDefinitionMessage>();

            foreach (var product in _soldProducts)
            {
                var filter = filterBuilder.Where(def => def.Id == product.Id&&def.Quantity>=product.Quantity);

                var updateDefinition = new UpdateDefinitionBuilder<ProductDefinition>().Inc(p => p.Quantity, -product.Quantity);

                var productDef = await _repository.UpdateAsync(session, filter, updateDefinition);
                if (productDef == null)
                {
                    errors.Add(new ProductDefinitionMessage(product.Id, product.Name, "No product in the specified quantity."));
                }
            }

            if (errors.Count > 0)
                throw new InvalidOperationException(Newtonsoft.Json.JsonConvert.SerializeObject(errors));
        }
    }

}