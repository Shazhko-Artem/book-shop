﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Cart;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb
{
    /// <summary>
    /// The Mongodb context
    /// </summary>
    public class MongodbContext: IMongodbContext
    {
        private readonly IMongoDatabase _database;
        private readonly MongoClient _client;
        /// <summary>
        /// The mongodb collection of products definitions
        /// </summary>
        public IMongoCollection<ProductDefinition> ProductDefinition => _database.GetCollection<ProductDefinition>("Products");
        /// <summary>
        /// The mongodb collection of orders
        /// </summary>
        public IMongoCollection<Order> Orders => _database.GetCollection<Order>("Orders");
        /// <summary>
        /// Create a new session
        /// </summary>
        /// <returns>New session</returns>
        public async Task<IClientSessionHandle> CreateSession()
        {
            return await _client.StartSessionAsync();
        }
        /// <summary>
        /// Initializes the MongodbContext
        /// </summary>
        public MongodbContext()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Mongodb"].ConnectionString;
            var connection = new MongoUrlBuilder(connectionString);

            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(connection.DatabaseName);
        }
    }
}