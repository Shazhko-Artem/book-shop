﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases.Commands;
using MongoDB.Bson;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb
{
    /// <summary>
    /// The Mongodb orders repository.
    /// </summary>
    public class MongodbOrderRepository : IMongodbRepository<Order>
    {
        private readonly IMongodbContext _context;
        /// <summary>
        /// Initializes the MongodbOrderRepository
        /// </summary>
        /// <param name="context">The Mongodb context</param>
        public MongodbOrderRepository(IMongodbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get order by id
        /// </summary>
        /// <param name="id">The order id</param>
        /// <returns>Found order</returns>
        public async Task<Order> GetByIdAsync(string id)
        {
            return (await _context.Orders.FindAsync<Order>(new BsonDocument("_id", id))).FirstOrDefault();
        }
        /// <summary>
        /// Get all orders
        /// </summary>
        /// <returns>All orders in the repository</returns>
        public async Task<IList<Order>> GetAllAsync()
        {
            return await (await _context.Orders.FindAsync<Order>(new BsonDocument())).ToListAsync();
        }
        /// <summary>
        /// Add new order to repository
        /// </summary>
        /// <param name="order">Order for addition</param>
        /// <returns>Added order</returns>
        public async Task<Order> AddAsync(Order order)
        {
            await _context.Orders.InsertOneAsync(order);
            return order;
        }
        /// <summary>
        /// Remove order in repository
        /// </summary>
        /// <param name="order">Order to remove</param>
        /// <returns>Operation result</returns>
        public async Task<bool> RemoveAsync(Order order)
        {
            var result = await _context.Orders.DeleteOneAsync<Order>(history => history.Id == order.Id);
            return result.DeletedCount > 0;
        }
        /// <summary>
        /// Receive a visitor to execute the command
        /// </summary>
        /// <param name="visitor">Visitor</param>
        public async Task AcceptAsync(IRepositoryCommandVisitor<Order> visitor)
        {
            await visitor.VisitAsync(this);
        }
     
        /// <summary>
        /// Update order by filter in session
        /// </summary>
        /// <param name="session">The session</param>
        /// <param name="filter">The filter</param>
        /// <param name="updateDefinition">The update definition</param>
        /// <returns>Updated order</returns>
        public async Task<Order> UpdateAsync(IClientSessionHandle session, FilterDefinition<Order> filter, UpdateDefinition<Order> updateDefinition)
        {
            var options = new FindOneAndUpdateOptions<Order, Order>() { IsUpsert = false, ReturnDocument = ReturnDocument.After };
            return await _context.Orders.FindOneAndUpdateAsync(session, filter, updateDefinition, options);
        }
        /// <summary>
        /// Create a new session
        /// </summary>
        /// <returns>New session</returns>
        public async Task<IClientSessionHandle> CreateSessionAsync()
        {
            return await _context.CreateSession();
        }
    }
}