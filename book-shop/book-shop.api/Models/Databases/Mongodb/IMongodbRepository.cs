﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb
{
    /// <summary>
    /// The Mongodb repository interface
    /// </summary>
    public interface IMongodbRepository<T>:IRepository<T>
    {
        /// <summary>
        /// Update products by filter in session
        /// </summary>
        /// <param name="session">The session</param>
        /// <param name="filter">The filter</param>
        /// <param name="updateDefinition">The update definition</param>
        /// <returns>Updated product definition</returns>
        Task<T> UpdateAsync(IClientSessionHandle session, FilterDefinition<T> filter,
            UpdateDefinition<T> updateDefinition);
        /// <summary>
        /// Create a new session
        /// </summary>
        /// <returns>New session</returns>
        Task<IClientSessionHandle> CreateSessionAsync();
    }
}
