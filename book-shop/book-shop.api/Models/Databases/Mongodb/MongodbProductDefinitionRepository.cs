﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Databases.Commands;
using MongoDB.Bson;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb
{
    /// <summary>
    /// The Mongodb product definition repository.
    /// </summary>
    public class MongodbProductDefinitionRepository: IMongodbRepository<ProductDefinition>
    {
        private readonly IMongodbContext _context;
        /// <summary>
        /// Initializes the MongodbProductDefinitionRepository
        /// </summary>
        /// <param name="context">The Mongodb context</param>
        public MongodbProductDefinitionRepository(IMongodbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns>All products in the repository</returns>
        public async Task<IList<ProductDefinition>> GetAllAsync()
        {
            return await (await _context.ProductDefinition.FindAsync<ProductDefinition>(new BsonDocument())).ToListAsync();
        }
        /// <summary>
        /// Add new product to repository
        /// </summary>
        /// <param name="product">Product for addition</param>
        /// <returns>Added product</returns>
        public async Task<ProductDefinition> AddAsync(ProductDefinition product)
        {
            await _context.ProductDefinition.InsertOneAsync(product);
            return product;
        }
        /// <summary>
        /// Remove product in repository
        /// </summary>
        /// <param name="product">Product to remove</param>
        /// <returns>Operation result</returns>
        public async Task<bool> RemoveAsync(ProductDefinition product)
        {
            var result = await _context.ProductDefinition.DeleteOneAsync<ProductDefinition>(definition => definition.Id == product.Id);
            return result.DeletedCount > 0;
        }
        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="id">The product id</param>
        /// <returns>Found product</returns>
        public async Task<ProductDefinition> GetByIdAsync(string id)
        {
            return await (await _context.ProductDefinition.FindAsync<ProductDefinition>(new BsonDocument("_id", new ObjectId(id)))).FirstOrDefaultAsync();
        }
        /// <summary>
        /// Receive a visitor to execute the command
        /// </summary>
        /// <param name="visitor">Visitor</param>
        public async Task AcceptAsync(IRepositoryCommandVisitor<ProductDefinition> visitor)
        {
            await visitor.VisitAsync(this);
        }
        /// <summary>
        /// Update products by filter in session
        /// </summary>
        /// <param name="session">The session</param>
        /// <param name="filter">The filter</param>
        /// <param name="updateDefinition">The update definition</param>
        /// <returns>Updated product definition</returns>
        public async Task<ProductDefinition> UpdateAsync(IClientSessionHandle session, FilterDefinition<ProductDefinition> filter,
            UpdateDefinition<ProductDefinition> updateDefinition)
        {
            var options = new FindOneAndUpdateOptions<ProductDefinition, ProductDefinition>() { IsUpsert = false, ReturnDocument = ReturnDocument.After };
            return await _context.ProductDefinition.FindOneAndUpdateAsync(session, filter, updateDefinition, options);
        }
        /// <summary>
        /// Create a new session
        /// </summary>
        /// <returns>New session</returns>
        public async Task<IClientSessionHandle> CreateSessionAsync()
        {
            return await _context.CreateSession();
        }
    }
}