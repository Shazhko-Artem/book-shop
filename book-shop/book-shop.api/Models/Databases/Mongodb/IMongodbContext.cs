﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Cart;
using MongoDB.Driver;

namespace book_shop.api.Models.Databases.Mongodb
{
    /// <summary>
    /// The Mongodb context interface
    /// </summary>
    public interface IMongodbContext
    {
        /// <summary>
        /// The mongodb collection of products definitions
        /// </summary>
        IMongoCollection<ProductDefinition> ProductDefinition { get; }
        /// <summary>
        /// The mongodb collection of orders
        /// </summary>
        IMongoCollection<Order> Orders { get; }
        /// <summary>
        /// Create a new session
        /// </summary>
        /// <returns>A new session</returns>
        Task<IClientSessionHandle> CreateSession();
    }
}
