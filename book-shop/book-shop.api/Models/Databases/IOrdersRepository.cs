﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Cart;

namespace book_shop.api.Models.Databases
{
    public interface IOrdersRepository
    {
        Task<IList<Order>> GetAllAsync();
        Task<Order> AddAsync(Order saleHistory);
        Task<bool> RemoveAsync(Order saleHistory);
    }
}
