﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.api.Models.Databases
{
    /// <summary>
    /// Message the product.
    /// </summary>
    public class ProductDefinitionMessage
    {
        /// <summary>
        /// The product id
        /// </summary>
        public string ProductId { get; }
        /// <summary>
        /// The product name
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// The message
        /// </summary>
        public string Message { get; }
        /// <summary>
        /// Initializes a new ProductDefinitionMessage
        /// </summary>
        /// <param name="productId">The product id</param>
        /// <param name="name">The product name</param>
        /// <param name="message">The product message</param>
        public ProductDefinitionMessage(string productId, string name,string message)
        {
            ProductId = productId;
            Name = name;
            Message = message;
        }
    }
}