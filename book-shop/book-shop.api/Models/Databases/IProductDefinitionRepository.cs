﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.api.Models.Databases
{
    /// <summary>
    /// The product description repository interface for work with database
    /// </summary>
    public interface IProductDefinitionRepository
    {
        /// <summary>
        /// Get all products definitions
        /// </summary>
        /// <returns></returns>
        Task<IList<ProductDefinition>> GetAllAsync();
        /// <summary>
        /// Get all products definitions
        /// </summary>
        /// <returns></returns>
        Task<ProductDefinition> GetByIdAsync(string id);
    }
}
