﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Databases.Mongodb;

namespace book_shop.api.Models.Databases.Commands
{
    /// <summary>
    /// Basic repository visitor interface
    /// </summary>
    public interface IRepositoryCommandVisitor<T>
    {
        /// <summary>
        /// Visit repository for run command
        /// </summary>
        /// <param name="repository">The repository</param>
        Task VisitAsync(IRepository<T> repository);
        /// <summary>
        /// Visit Mongodb repository for run command 
        /// </summary>
        /// <param name="repository"></param>
        Task VisitAsync(IMongodbRepository<T> repository);
    }
}
