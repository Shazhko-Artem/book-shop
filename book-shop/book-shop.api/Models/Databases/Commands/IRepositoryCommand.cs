﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.api.Models.Databases.Commands
{
    /// <summary>
    /// Command for repository
    /// </summary>
    public interface IRepositoryCommand
    {
        /// <summary>
        /// Run command
        /// </summary>
        Task ExecuteAsync();
    }
}
