﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management.Instrumentation;
using System.Threading.Tasks;
using System.Web;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases.Mongodb;
using book_shop.api.Models.Databases.Mongodb.Commands;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace book_shop.api.Models.Databases.Commands
{
    /// <summary>
    /// Visitor for decrement the quantity products in repository
    /// </summary>
    public class DecrementProductQuantityCommandVisitor : Databases.Commands.IRepositoryCommandVisitor<ProductDefinition>,IEquatable<DecrementProductQuantityCommandVisitor>
    {
        private readonly ICollection<SoldProduct> _soldProducts;

        /// <summary>
        /// Initializes the DecrementProductQuantityCommandVisitor
        /// </summary>
        /// <param name="soldProducts">Products for sold</param>
        public DecrementProductQuantityCommandVisitor(ICollection<SoldProduct> soldProducts)
        {
            _soldProducts = soldProducts;
        }
        /// <summary>
        /// Visit repository for run command
        /// </summary>
        /// <param name="repository">The product definition repository</param>
        public Task VisitAsync(IRepository<ProductDefinition> repository)
        {
            throw new InstanceNotFoundException();
        }
        /// <summary>
        /// Visit mongodb repository for run command
        /// </summary>
        /// <param name="repository">The product definition mongodb repository</param>
        public async Task VisitAsync(IMongodbRepository<ProductDefinition> repository)
        {
            Databases.Mongodb.Commands.DecrementProductQuantityCommand command =
                new Mongodb.Commands.DecrementProductQuantityCommand(_soldProducts, repository);
            await command.ExecuteAsync();
        }

        public bool Equals(DecrementProductQuantityCommandVisitor other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _soldProducts.SequenceEqual(other._soldProducts);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DecrementProductQuantityCommandVisitor) obj);
        }

        public override int GetHashCode()
        {
            return (_soldProducts != null ? _soldProducts.GetHashCode() : 0);
        }
    }
}