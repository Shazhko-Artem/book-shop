﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using book_shop.api.Models.Databases;
using Newtonsoft.Json;

namespace book_shop.api.Controllers
{
    /// <summary>
    /// Product api controller
    /// </summary>
    public class ProductsController : ApiController
    {
        private readonly IRepository<ProductDefinition> _repository;
        /// <summary>
        /// Initializes the ProductsController
        /// </summary>
        /// <param name="repository">Products repository</param>
        public ProductsController(IRepository<ProductDefinition> repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Get products collection
        /// </summary>
        /// <returns>Products collection</returns>
        [System.Web.Mvc.HttpGet]
        public async Task<HttpResponseMessage> GetProductsAsync()
        {
            IList<ProductDefinition> definitions = await _repository.GetAllAsync();
            return Request.CreateResponse(HttpStatusCode.OK, new{Count= definitions.Count,Products= definitions});
        }
        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="id">Product id</param>
        /// <returns>Found product</returns>
        [System.Web.Mvc.HttpGet]
        public async Task<HttpResponseMessage> GetProductByIdAsync(string id)
        {
            if (id.Length != 24)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            ProductDefinition definition = await _repository.GetByIdAsync(id);
            if (definition == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            return Request.CreateResponse(HttpStatusCode.OK, await _repository.GetByIdAsync(id));
        }
    }
}