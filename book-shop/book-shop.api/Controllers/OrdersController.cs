﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;

namespace book_shop.api.Controllers
{
    /// <summary>
    /// Orders api controller
    /// </summary>
    public class OrdersController : ApiController
    {
        private readonly IRepository<Order> _ordersRepository;
        private readonly IRepository<ProductDefinition> _productsRepository;

        /// <summary>
        /// Initializes the OrdersController
        /// </summary>
        /// <param name="ordersRepository">Orders repository</param>
        /// <param name="productsRepository">Products repository</param>
        public OrdersController(IRepository<Models.Cart.Order> ordersRepository, IRepository<Models.Databases.ProductDefinition> productsRepository)
        {
            _ordersRepository = ordersRepository;
            _productsRepository = productsRepository;
        }
     
        /// <summary>
        /// Add a new order
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Operation result</returns>
        [HttpPut]
        public async Task<HttpResponseMessage> AddOrderAsync(Order order)
        {
            Cashier cashier =new Cashier(_ordersRepository,_productsRepository);
            Order orderResponse = null;
            try
            {
                orderResponse = await cashier.CheckoutAsync(order);
            }
            catch (InvalidOperationException operationException)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,new{Message= operationException.Message});
            }
            return Request.CreateResponse(HttpStatusCode.Created,orderResponse);
        }
        /// <summary>
        /// Get orders collection
        /// </summary>
        /// <returns>Operation result</returns>
        [HttpGet]
        public async Task<HttpResponseMessage> GetOrdersAsync()
        {
            IList<Order> orders = await _ordersRepository.GetAllAsync();
            return Request.CreateResponse(HttpStatusCode.OK, new { Count = orders.Count,Orders = orders});
        }
    }
}
