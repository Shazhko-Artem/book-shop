﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using book_shop.WebUI.Models;
using book_shop.WebUI.Models.Cart;
using book_shop.WebUI.Models.Databases;
using book_shop.WebUI.Models.Extensions;
using book_shop.WebUI.Models.Products;
using book_shop.WebUI.Models.States;
using Newtonsoft.Json;

namespace book_shop.WebUI.Controllers
{
    public class CartController : Controller
    {
        private readonly ICashier _cashier;
        private readonly IRepository<Product> _repository;
        private readonly ISessionObjectsFactory _sessionObjectFactory;

        /// <summary>
        /// Initializes the CartController
        /// </summary>
        /// <param name="repository">Products repository</param>
        /// <param name="cashier">Cashier</param>
        /// <param name="sessionObjectFactory">Provider for provide objects from the session</param>
        public CartController(IRepository<Product> repository, ICashier cashier,ISessionObjectsFactory sessionObjectFactory)
        {
            _cashier = cashier;
            _repository = repository;
            _sessionObjectFactory = sessionObjectFactory;
        }
        /// <summary>
        /// Get Index view with show cart content
        /// </summary>
        /// <param name="returnUrl">Return url</param>
        public async Task<ViewResult> Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                ReturnUrl = returnUrl,
                Cart = await _sessionObjectFactory.GetCartObject(this.ControllerContext)
            });
        }

        /// <summary>
        /// Add product to the cart
        /// </summary>
        /// <param name="productId">Product id</param>
        /// <param name="returnUrl">Return url</param>
        public async Task<RedirectToRouteResult> AddToCart(string productId,
            string returnUrl)
        {
            ICart cart = await _sessionObjectFactory.GetCartObject(this.ControllerContext);

            Product product = await _repository.GetByIdAsync(productId);

            if (product != null)
            {
                var soldProduct = cart.SoldProducts.FirstOrDefault(p=> p.Id == product.Id);
                
                if(soldProduct==null || soldProduct.Quantity<product.Quantity)
                    cart.Add(product, 1);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        /// <summary>
        /// Set quantity products in the cart
        /// </summary>
        /// <param name="productId">Product id</param>
        /// <param name="quantity">Product quantity for sold</param>
        [HttpPost]
        public async Task<string> SetQuantityProductInCart(string productId, int quantity)
        {
            if (quantity < 1)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "The number of products can not be less than 1.";
            }

            ICart cart = await _sessionObjectFactory.GetCartObject(this.ControllerContext);

            Product product = await _repository.GetByIdAsync(productId);
            if (product != null)
            {
                var soldProduct = cart.SoldProducts.FirstOrDefault(p => p.Id == product.Id);
                if (quantity <= product.Quantity)
                {
                    cart.SetQuantity(product, quantity);
                    return quantity.ToString();
                }

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return $"No product in the specified quantity. There are only {product.Quantity} in stock.";
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return "Not found product.";
        }
        /// <summary>
        /// Remove the product from the cart
        /// </summary>
        /// <param name="productId">Product id</param>
        [HttpDelete]
        public async Task RemoveFromCart(string productId)
        {
            Product product = await _repository.GetByIdAsync(productId);

            ICart cart = await _sessionObjectFactory.GetCartObject(this.ControllerContext);

            if (product != null)
            {
                cart.Remove(product);
            }
        }

        /// <summary>
        /// Get partial view for cart context
        /// </summary>
        /// <param name="cart">Cart</param>
        public PartialViewResult GetCartData()
        {
            var errors = TempData["Cart_Errors"] as Dictionary<string, string>;
            if (errors != null)
            {
                foreach (var error in errors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            ICart cart = _sessionObjectFactory.GetCartObject(this.ControllerContext).Result;

            return PartialView(cart);
        }
        /// <summary>
        /// Get 'Checkout' view for input contact data
        /// </summary>
        /// <param name="returnUrl">Return url</param>
        [HttpGet]
        public ViewResult Checkout(string returnUrl)
        {
            return View(new CheckoutViewModel(){ShoppingDetails=new ShoppingDetails(),ReturnUrl = returnUrl});
        }
        /// <summary>
        /// Create new order
        /// </summary>
        /// <param name="viewModel">View model</param>
        [HttpPost]
        public async Task<ActionResult> Checkout(CheckoutViewModel viewModel)
        {
            ICart cart = await _sessionObjectFactory.GetCartObject(this.ControllerContext);

            if (cart == null || cart.SoldProducts == null || !cart.SoldProducts.Any())
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _cashier.CheckoutAsync(cart, viewModel.ShoppingDetails);
                    cart.Clear();
                    return View("Completed");
                }
                catch (Exception e)
                {
                    try
                    {
                        var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(e.Message);
                        var errorProducts = JsonConvert.DeserializeObject<List<ProductError>>(errorResponse.Message);

                        Dictionary<string, string> errors = new Dictionary<string, string>();
                        foreach (var errorProduct in errorProducts)
                        {
                            errors.Add(
                                $"Lines[{cart.SoldProducts.FindIndex(p => p.Id == errorProduct.ProductId)}].Quantity",
                                errorProduct.Message);
                        }

                        TempData["Cart_Errors"] = errors;
                        return View("Index", new CartIndexViewModel
                        {
                            ReturnUrl = viewModel.ReturnUrl,
                            Cart = cart
                        });
                    }
                    catch
                    {
                        return View("Error");
                    }
                }
            }
            else
            {
                return View(viewModel);
            }
        }

        /// <summary>
        /// Get partial view for summary cart
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public PartialViewResult Summary(string returnUrl)
        {
            ICart cart = _sessionObjectFactory.GetCartObject(this.ControllerContext).Result;

            return PartialView(new CartIndexViewModel(){Cart = cart, ReturnUrl = returnUrl});
        }
    }
}