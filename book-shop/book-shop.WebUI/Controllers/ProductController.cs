﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using book_shop.WebUI.Models;
using book_shop.WebUI.Models.Databases;
using book_shop.WebUI.Models.Pages;
using book_shop.WebUI.Models.Products;

namespace book_shop.WebUI.Controllers
{
    /// <summary>
    /// Product controller
    /// </summary>
    public class ProductController : Controller
    {
        private readonly IRepository<Product> _repository;
        private int _pageSize = 4;

        /// <summary>
        /// Initializes the ProductController
        /// </summary>
        /// <param name="repository">Product repository</param>
        public ProductController(IRepository<Product> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get 'List' view for show product collection
        /// </summary>
        /// <param name="page">Page number</param>
        public async Task<ViewResult> List(int page = 1)
        {
            IList<Product> products = await _repository.GetAllAsync();
            ProductsListViewModel viewModel = new ProductsListViewModel()
            {
                PagingInfo = new PagingInfo() { ItemsPerPage = _pageSize, TotalItems = products.Count, CurrentPage = page },
                Products = products.Skip((page - 1) * _pageSize).Take(_pageSize),
            };
            return View(viewModel);
        }
        /// <summary>
        /// Get 'GetProduct' view for show product detail info
        /// </summary>
        /// <param name="id">Product id</param>
        public async Task<ViewResult> GetProduct(string id)
        {
            Product products = await _repository.GetByIdAsync(id);
            return View(products);
        }
    }
}