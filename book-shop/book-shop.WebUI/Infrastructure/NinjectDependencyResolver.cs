﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using book_shop.WebUI.Models.Cart;
using book_shop.WebUI.Models.Databases;
using book_shop.WebUI.Models.Products;
using book_shop.WebUI.Models.States;
using Ninject;
using Ninject.Web.Common;

namespace book_shop.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }


        private void AddBindings()
        {
            _kernel.Bind<IRepository<Product>>().To<ProductRepository>().InRequestScope();
            _kernel.Bind<ISessionProvider>().To<RedisSessionsProvider>().InSingletonScope();
            _kernel.Bind<ISessionObjectsFactory>().To<SessionObjectFactory>().InSingletonScope();
            _kernel.Bind<ICashier>().To<Cashier>().InRequestScope();
        }
    }
}