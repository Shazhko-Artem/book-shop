﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using book_shop.WebUI.Models.Pages;

namespace book_shop.WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        /// <summary>
        /// Build html page links
        /// </summary>
        /// <param name="html">Html helper</param>
        /// <param name="pagingInfo">Pages info</param>
        /// <param name="pageUrl">Action for generate links</param>
        /// <returns></returns>
        public static MvcHtmlString PageLinks(this HtmlHelper html,
            PagingInfo pagingInfo,
            Func<int, string> pageUrl)
        {

            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}