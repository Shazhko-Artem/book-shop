﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models
{
    public class ErrorResponse
    {
        public string Message { get; set; }
    }
}