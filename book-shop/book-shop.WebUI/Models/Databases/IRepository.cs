﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.WebUI.Models.Databases
{
    /// <summary>
    /// Basic repository interface
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// Get item by id
        /// </summary>
        /// <param name="id">The item id</param>
        /// <returns>Found item</returns>
        Task<T> GetByIdAsync(string id);
        /// <summary>
        /// Get all items
        /// </summary>
        /// <returns>All items in the repository</returns>
        Task<IList<T>> GetAllAsync();
        /// <summary>
        /// Add new item to repository
        /// </summary>
        /// <param name="item">Item for addition</param>
        /// <returns>Added item</returns>
        Task<T> AddAsync(T item);
        /// <summary>
        /// Remove item in repository
        /// </summary>
        /// <param name="item">Item to remove</param>
        /// <returns>Operation result</returns>
        Task<bool> RemoveAsync(T item);
    }
}
