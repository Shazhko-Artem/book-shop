﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using book_shop.WebUI.Models.Products;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace book_shop.WebUI.Models.Databases
{
    /// <summary>
    /// Product repository. Class for remote work with book-shop.api
    /// </summary>
    public sealed class ProductRepository:IRepository<Product>
    {
        /// <summary>
        /// Class for deserialization response from book-shop.api
        /// </summary>
        private class ProductsResponse
        {
            /// <summary>
            /// Count products
            /// </summary>
            public int Count { get; set; }
            /// <summary>
            /// Products collection
            /// </summary>
            public IList<Product> Products { get; set; }
        }

        private readonly string _connectionString;

        /// <summary>
        /// Initializes the ProductRepository
        /// </summary>
        public ProductRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["WebApi"].ConnectionString;
        }
        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="id">The product id</param>
        /// <returns>Found product</returns>
        public async Task<Product> GetByIdAsync(string id)
        {
            using (var client=new HttpClient())
            {
                var response= await client.GetAsync($"{_connectionString}/Products/{id}");
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<Product>();
            }
        }
        /// <summary>
        /// Get all items
        /// </summary>
        /// <returns>All products in the repository</returns>
        public async Task<IList<Product>> GetAllAsync()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{_connectionString}/Products");
                response.EnsureSuccessStatusCode();
                return (await response.Content.ReadAsAsync<ProductsResponse>()).Products?? new List<Product>();
            }
        }
        /// <summary>
        /// Add new product to repository
        /// </summary>
        /// <param name="item">Product for addition</param>
        /// <returns>Added product</returns>
        public async Task<Product> AddAsync(Product item)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PutAsJsonAsync($"{_connectionString}/Products",item);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<Product>();
            }
        }
        /// <summary>
        /// Remove product in repository
        /// </summary>
        /// <param name="item">Product to remove</param>
        /// <returns>Operation result</returns>
        public async Task<bool> RemoveAsync(Product item)
        {
            using (var client = new HttpClient())
            {
                var response = await client.DeleteAsync($"{_connectionString}/Products/{item.Id}");
                response.EnsureSuccessStatusCode();
                return response.IsSuccessStatusCode;
            }
        }
    }
}