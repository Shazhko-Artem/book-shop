﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using StackExchange.Redis;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Redis session
    /// </summary>
    public class RedisSession:ISession
    {
        private readonly IDatabase _db;
        private readonly string _id;

        public RedisSession(string id,IDatabase db)
        {
            _id = id;
            _db = db;
        }

        /// <summary>
        /// Save value to session
        /// </summary>
        /// <param name="value">Value for save</param>
        public void Save(string value)
        {
            StateCheck();
            _db.StringSet(_id, value);
            SetTimeout();
        }
        /// <summary>
        /// Load state from session
        /// </summary>
        /// <returns>Session sate</returns>
        public string Load()
        {
            StateCheck();
            SetTimeout();
            return _db.StringGet(_id);
        }
        /// <summary>
        /// Set timeout for key
        /// </summary>
        private void SetTimeout()
        {
            _db.KeyExpire(_id, new TimeSpan(30, 0, 0, 0, 0));
        }
        /// <summary>
        /// Status check for validity
        /// </summary>
        private void StateCheck()
        {
            if (_db == null)
            {
                throw new SessionException("Session must be open");
            }
        }
    }
}