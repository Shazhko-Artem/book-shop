﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using book_shop.WebUI.Models.Cart;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Interface for provide objects from the session
    /// </summary>
    public interface ISessionObjectsFactory
    {
        /// <summary>
        /// Get the Cart object
        /// </summary>
        /// <param name="controllerContext">Controller context for getting a session id from the request</param>
        /// <returns>Cart object</returns>
        Task<ICart> GetCartObject(ControllerContext controllerContext);
    }
}
