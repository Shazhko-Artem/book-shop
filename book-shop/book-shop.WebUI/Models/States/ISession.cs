﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Basic session interface
    /// </summary>
    public interface ISession
    {
        /// <summary>
        /// Save value to session
        /// </summary>
        /// <param name="value">Value for save</param>
        void Save(string value);
        /// <summary>
        /// Load state from session
        /// </summary>
        /// <returns>Session sate</returns>
        string Load();
    }
}
