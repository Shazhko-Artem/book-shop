﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Session exception
    /// </summary>
    public class SessionException:Exception
    {
        /// <summary>
        /// Initializes the SessionException
        /// </summary>
        /// <param name="message">Error message</param>
        public SessionException(string message):base(message){}
    }
}