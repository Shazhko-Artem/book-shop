﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Interface for provide a session
    /// </summary>
    public interface ISessionProvider
    {
        /// <summary>
        /// Get session by session ID in controller context
        /// </summary>
        /// <param name="controllerContext">Controller context for getting a session id from the request</param>
        /// <returns></returns>
        Task<ISession> GetSessionAsync(ControllerContext controllerContext);
    }
}
