﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using book_shop.WebUI.Models.Cart;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Class for retrieving objects from the session
    /// </summary>
    public class SessionObjectFactory:ISessionObjectsFactory
    {
        private readonly ISessionProvider _sessionProvider;

        /// <summary>
        /// Initializes the SessionObjectFactory
        /// </summary>
        /// <param name="sessionProvider">Session provider for get session</param>
        public SessionObjectFactory(ISessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        /// <summary>
        /// Get the Cart object
        /// </summary>
        /// <param name="controllerContext">Controller context for getting a session id from the request</param>
        /// <returns>Cart object</returns>
        public async Task<ICart> GetCartObject(ControllerContext controllerContext)
        {
            ISession session = await _sessionProvider.GetSessionAsync(controllerContext).ConfigureAwait(false);

            var proxy = new CartProxy(session);
            proxy.Load();
            return proxy;
        }
    }
}