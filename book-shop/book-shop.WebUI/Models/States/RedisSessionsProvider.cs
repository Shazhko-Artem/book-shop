﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using StackExchange.Redis;

namespace book_shop.WebUI.Models.States
{
    /// <summary>
    /// Class to provide redis session
    /// </summary>
    public class RedisSessionsProvider:ISessionProvider
    {
        private readonly ConnectionMultiplexer _redis;
        private const string SessionKey = "session_id";

        public RedisSessionsProvider()
        {
            string connectionString=System.Configuration.ConfigurationManager.ConnectionStrings["Redis"].ConnectionString;
            _redis = ConnectionMultiplexer.Connect(connectionString);
        }
        /// <summary>
        /// Get redis session by session ID in controller context
        /// </summary>
        /// <param name="controllerContext">Controller context for getting a session id from the request</param>
        /// <returns>Session of </returns>
        public async Task<ISession> GetSessionAsync(ControllerContext controllerContext)
        {
            IDatabase database= _redis.GetDatabase();
            string sessionId = await LoadSessionId(controllerContext, database).ConfigureAwait(false);

            return new RedisSession(sessionId,database);
        }

        /// <summary>
        /// Load session id from the request or create new a session id
        /// </summary>
        /// <param name="controllerContext">Controller context for getting a session id from the request</param>
        /// <param name="database">Database for check the exist of ID</param>
        /// <returns>Session id</returns>
        private async Task<string> LoadSessionId(ControllerContext controllerContext,IDatabase database)
        {
            string sessionId = string.Empty;
            if (controllerContext.HttpContext.Request.Cookies[SessionKey] != null)
                sessionId = controllerContext.HttpContext.Request.Cookies[SessionKey].Value;

            if (string.IsNullOrEmpty(sessionId)|| !await database.KeyExistsAsync(sessionId).ConfigureAwait(false))
            {
                sessionId = System.Guid.NewGuid().ToString();
            }

            HttpCookie cookie = new HttpCookie(SessionKey)
            {
                Value = sessionId
            };
            controllerContext.HttpContext.Response.Cookies.Add(cookie);

            return sessionId;
        }
    }
}