﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Basic interface for create order
    /// </summary>
    public interface ICashier
    {
        /// <summary>
        /// Create order
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="shoppingDetails">Contact data</param>
        /// <returns>Created order</returns>
        Task<Order> CheckoutAsync(ICart cart, ShoppingDetails shoppingDetails);
    }
}
