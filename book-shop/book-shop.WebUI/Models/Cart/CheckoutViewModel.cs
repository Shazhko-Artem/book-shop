﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// ViewModel for 'Checkout' view of Cart
    /// </summary>
    public class CheckoutViewModel
    {
        /// <summary>
        /// Cart
        /// </summary>
        public ICart Cart { get; set; }
        /// <summary>
        /// Contact info
        /// </summary>
        public ShoppingDetails ShoppingDetails { get; set; }
        /// <summary>
        /// Return url
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}