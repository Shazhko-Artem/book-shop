﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Order description
    /// </summary>
    public class Order : IEquatable<Order>
    {
        /// <summary>
        /// The order id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The contact data
        /// </summary>
        public ShoppingDetails ShoppingDetails { get; set; }
        /// <summary>
        /// Sold products collection
        /// </summary>
        public List<SoldProduct> SoldProducts { get; set; }
        /// <summary>
        /// Initializes the Order
        /// </summary>
        public Order()
        {
        }

        /// <summary>
        /// Initializes the Order
        /// </summary>
        /// <param name="soldProducts">Sold products collection</param>
        /// <param name="shoppingDetails">Contact data</param>
        public Order(ICollection<SoldProduct> soldProducts, ShoppingDetails shoppingDetails)
        {
            SoldProducts = new List<SoldProduct>(soldProducts);
            ShoppingDetails = shoppingDetails;
        }
        /// <summary>
        /// Initializes the Order by copy other Order
        /// </summary>
        /// <param name="other">Other order</param>
        public Order(Order other)
        {
            Id = other.Id;
            ShoppingDetails = other.ShoppingDetails;
            SoldProducts = other.SoldProducts;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Order)obj);
        }
        public bool Equals(Order other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id)
                   && Equals(ShoppingDetails, other.ShoppingDetails)
                   && SoldProducts.SequenceEqual(SoldProducts);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ShoppingDetails != null ? ShoppingDetails.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SoldProducts != null ? SoldProducts.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}