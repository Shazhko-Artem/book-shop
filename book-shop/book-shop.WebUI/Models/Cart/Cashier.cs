﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Class for create order
    /// </summary>
    public class Cashier : ICashier
    {
        private readonly string _connectionString;

        /// <summary>
        /// Initializes the Cashier
        /// </summary>
        public Cashier()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["WebApi"].ConnectionString;
        }
        /// <summary>
        /// Create order
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="shoppingDetails">Contact data</param>
        /// <returns>Created order</returns>
        public async Task<Order> CheckoutAsync(ICart cart, ShoppingDetails shoppingDetails)
        {
            if (cart.SoldProducts == null || cart.SoldProducts.Count == 0)
                throw new ArgumentException("The sold products list is empty.");
            if (shoppingDetails == null)
                throw new ArgumentException("Shopping details is null");

            Order order = new Order() { ShoppingDetails = shoppingDetails, SoldProducts = cart.SoldProducts.ToList()};

            using (var client = new HttpClient())
            {
                var response = await client.PutAsJsonAsync($"{_connectionString}/Orders", order);
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<Order>();
                else
                {
                    throw new InvalidOperationException(await response.Content.ReadAsStringAsync());
                }
            }
        }
    }
}