﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Contact data
    /// </summary>
    public class ShoppingDetails : IEquatable<ShoppingDetails>
    {
        /// <summary>
        /// First name
        /// </summary>
        [Display(Name = "First name")]
        [MinLength(2, ErrorMessage = "Fist name cannot be less than 2 symbols")]
        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        [Display(Name = "Last name")]
        [MinLength(2,ErrorMessage = "Last name cannot be less than 2 symbols")]
        [Required(ErrorMessage = "Please enter your last name")]
        public string LastName { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        [Display(Name = "Phone number")]
        [MinLength(9,ErrorMessage = "Phone number cannot be less than 9 symbols")]
        [MaxLength(11, ErrorMessage = "Phone number cannot be less than 11 symbols")]
        [RegularExpression("^(\\d+)$")]
        [Required(ErrorMessage = "Please enter your phone number")]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Address
        /// </summary>
        [MinLength(3, ErrorMessage = "Address cannot be less than 3 symbols")]
        [Required(ErrorMessage = "Please enter your address")]
        public string Address { get; set; }

        /// <summary>
        /// Initializes the ShoppingDetails
        /// </summary>
        public ShoppingDetails() { }
        /// <summary>
        /// Initializes the ShoppingDetails
        /// </summary>
        /// <param name="firstName">Fist name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="address">Address</param>
        /// <param name="phoneNumber">Phone number</param>
        public ShoppingDetails(string firstName, string lastName, string address, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            PhoneNumber = phoneNumber;
        }

        public bool Equals(ShoppingDetails other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && string.Equals(Address, other.Address) && string.Equals(PhoneNumber, other.PhoneNumber);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ShoppingDetails)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Address != null ? Address.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PhoneNumber != null ? PhoneNumber.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}