﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// ViewModel for 'Index' view of Cart
    /// </summary>
    public class CartIndexViewModel
    {
        /// <summary>
        /// The cart
        /// </summary>
        public ICart Cart { get; set; }
        /// <summary>
        /// Return url
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}