﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using book_shop.WebUI.Models.Products;
using book_shop.WebUI.Models.States;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Cart proxy for save the state to the session
    /// </summary>
    public class CartProxy:ICart
    {
        private readonly ISession _session;
        private Cart _cart;

        /// <summary>
        /// Initializes the CartProxy
        /// </summary>
        /// <param name="session">The session</param>
        public CartProxy(ISession session)
        {
            _session = session;
        }
        /// <summary>
        /// Get products for sold
        /// </summary>
        public IList<SoldProduct> SoldProducts => _cart.SoldProducts;
        /// <summary>
        /// Add product to cart for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        public void Add(Product product, int quantity)
        {
            _cart.Add(product,quantity);

            string json = JsonConvert.SerializeObject(_cart);
            _session.Save(json);
        }
        /// <summary>
        /// Set products quantity for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        public void SetQuantity(Product product, int quantity)
        {
            _cart.SetQuantity(product, quantity);

            string json = JsonConvert.SerializeObject(_cart);
            _session.Save(json);
        }
        /// <summary>
        /// Remove product from cart
        /// </summary>
        /// <param name="product">The product</param>
        public void Remove(Product product)
        {
            _cart.Remove(product);

            string json = JsonConvert.SerializeObject(_cart);
            _session.Save(json);
        }
        /// <summary>
        /// Compute total price
        /// </summary>
        /// <returns>Total price</returns>
        public decimal ComputeTotalValue()
        {
            return _cart.ComputeTotalValue();
        }
        /// <summary>
        /// Remove all items from the cart
        /// </summary>
        public void Clear()
        {
            _cart.Clear();

            string json = JsonConvert.SerializeObject(_cart);
            _session.Save(json);
        }
        /// <summary>
        /// Load cart from session
        /// </summary>
        public void Load()
        {
            string cartJson = _session.Load();
            if (!string.IsNullOrEmpty(cartJson))
                _cart = JsonConvert.DeserializeObject<Cart>(cartJson);

            if (_cart == null)
                _cart = new Cart();
        }
    }
}