﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.WebUI.Models.Products;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// The basic Cart interface
    /// </summary>
    public interface ICart
    {
        /// <summary>
        /// Get products for sold
        /// </summary>
        IList<SoldProduct> SoldProducts { get; }
        /// <summary>
        /// Add product to cart for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        void Add(Product product, int quantity);
        /// <summary>
        /// Set products quantity for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        void SetQuantity(Product product, int quantity);
        /// <summary>
        /// Remove product from cart
        /// </summary>
        /// <param name="product">The product</param>
        void Remove(Product product);
        /// <summary>
        /// Compute total price
        /// </summary>
        /// <returns>Total price</returns>
        decimal ComputeTotalValue();
        /// <summary>
        /// Remove all items from the cart
        /// </summary>
        void Clear();
    }
}
