﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using book_shop.WebUI.Models.Products;

namespace book_shop.WebUI.Models.Cart
{
    /// <summary>
    /// Cart with products for sold
    /// </summary>
    public class Cart:ICart
    {
        private readonly List<SoldProduct> _lineCollection = new List<SoldProduct>();
        /// <summary>
        /// Get products for sold
        /// </summary>
        public IList<SoldProduct> SoldProducts => _lineCollection;
        /// <summary>
        /// Add product to cart for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        public void Add(Product product, int quantity)
        {
            SoldProduct soldProduct = _lineCollection
                .FirstOrDefault(p => p.Id == product.Id);

            if (soldProduct == null)
            {
                _lineCollection.Add(new SoldProduct(product.Id, product.Name, quantity, product.Price));
            }
            else
            {
                soldProduct.Quantity += quantity;
            }
        }
        /// <summary>
        /// Set products quantity for sold
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="quantity">Quantity of products</param>
        public void SetQuantity(Product product, int quantity)
        {
            var soldProduct = SoldProducts.FirstOrDefault(p => p.Id == product.Id);
            if (soldProduct != null)
            {
                soldProduct.Quantity = quantity;
            }
            else
            {
                Add(product, quantity);
            }
        }
        /// <summary>
        /// Remove product from cart
        /// </summary>
        /// <param name="product">The product</param>
        public void Remove(Product product)
        {
            _lineCollection.RemoveAll(p => p.Id == product.Id);
        }
        /// <summary>
        /// Compute total price
        /// </summary>
        /// <returns>Total price</returns>
        public decimal ComputeTotalValue()
        {
            return _lineCollection.Sum(e => e.Price * e.Quantity);

        }
        /// <summary>
        /// Remove all items from the cart
        /// </summary>
        public void Clear()
        {
            _lineCollection.Clear();
        }
    }
}