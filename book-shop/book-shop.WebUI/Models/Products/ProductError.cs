﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace book_shop.WebUI.Models.Products
{
    /// <summary>
    /// Product error description
    /// </summary>
    public class ProductError
    {
        /// <summary>
        /// The product id
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// The name product
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The error message
        /// </summary>
        public string Message { get; set; }
    }
}