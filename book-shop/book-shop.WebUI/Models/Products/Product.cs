﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace book_shop.WebUI.Models.Products
{
    public class Product : IEquatable<Product>
    {
        /// <summary>
        /// The product id
        /// </summary
        public string Id { get; set; }
        /// <summary>
        /// The product name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The category name
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// The quantity of products
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// The price of products
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// The specifics fields collections
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, JToken> Expansion { get; set; }

        public bool Equals(Product other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id) && string.Equals(Name, other.Name) && string.Equals(Category, other.Category) && Quantity == other.Quantity && Price.Equals(other.Price);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Product)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Category != null ? Category.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Quantity;
                hashCode = (hashCode * 397) ^ (Expansion != null ? Expansion.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}