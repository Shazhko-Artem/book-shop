﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using book_shop.WebUI.Models.Pages;

namespace book_shop.WebUI.Models.Products
{
    /// <summary>
    /// ViewModel for 'List' view in Product
    /// </summary>
    public class ProductsListViewModel
    {
        /// <summary>
        /// Products collection
        /// </summary>
        public IEnumerable<Product> Products { get; set; }
        /// <summary>
        /// Pages info
        /// </summary>
        public PagingInfo PagingInfo { get; set; }
    }
}