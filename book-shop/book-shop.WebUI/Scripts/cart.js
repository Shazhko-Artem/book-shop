﻿function setMinIfNull(obj) {
    let value = parseInt(obj.value);
    if (!value) obj.value = obj.min;

    return true;
}

function inputQuantityChanged(obj) {
    setMinIfNull(obj);
    $("#checkoutNow_button_id").addClass("disabled");

    $.ajax("SetQuantityProductInCart",
        {
            type: "POST",
            success: function (data) {
                getCartSummary();
                getCartList();
            },
            data: {
                productId: $(obj).attr("data-product-id"),
                quantity: obj.value
            },
            error: function errorQuantity(qXHR, textStatus, errorThrown) {
                $(obj).addClass("input-validation-error").attr({
                    title: qXHR.responseText,
                    "data-toggle": "tooltip",
                    "data-placement":"top"
                });
            }
        });
}

function getReturnUrlForCart() {
    let urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("returnUrl");
}

function getCartList() {
    $.ajax("GetCartData",
        {
            type: "GET",
            success: function (data) {
                $("#cartTable").html(data);
                if ($("tr", data).length < 3) {
                    $("#checkoutNow_button_id").addClass("disabled");
                } else {
                    $("#checkoutNow_button_id").removeClass("disabled");
                }
            }
        });
}

function removeProductFromCart(obj) {

    $("#checkoutNow_button_id").addClass("disabled");

    $.ajax("RemoveFromCart",
        {
            type: "DELETE",
            success: function (data) {
                getCartSummary();
                getCartList();
            },
            data: {
                productId: $(obj).attr("data-product-id"),
            }
        });
}

function checkoutNowClick() {
    if ($(".input-validation-error").length) {
        return false;
    }
    return true;
}

function getCartSummary() {
    $.ajax("Summary",
        {
            type: "GET",
            success: function (data) {
                $("#cart_summary_id").html(data);
            },
            data: {
                 returnUrl:getReturnUrlForCart()
            }
        });
}