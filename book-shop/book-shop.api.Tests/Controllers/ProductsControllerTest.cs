﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using book_shop.api.Controllers;
using book_shop.api.Models.Databases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace book_shop.api.Tests.Controllers
{
    [TestClass]
    public class ProductsControllerTest
    {
        private Moq.Mock<IRepository<ProductDefinition>> _productRepositoryMock;
        private List<ProductDefinition> _productDefinitionsList;
        [TestInitialize]
        public void InitTest()
        {
            _productRepositoryMock = new Mock<IRepository<ProductDefinition>>();
            _productDefinitionsList = new List<ProductDefinition>()
            {
                new ProductDefinition(){Name = "ASP.NET MVC5", Id = "123456789123456789123456", Category = "Books",Quantity = 5,Price = 1023},
                new ProductDefinition(){Name = "ASP.NET MVC4", Id = "223456789123456789123456", Category = "Books",Quantity = 1,Price = 1013},
                new ProductDefinition(){Name = "ASP.NET MVC3", Id = "323456789123456789123456", Category = "Books",Quantity = 2,Price = 1000}
            };
        }

        [TestMethod]
        public async Task GetProducts_GetAllProducts()
        {
            _productRepositoryMock.Setup(repository => repository.GetAllAsync()).ReturnsAsync(() => _productDefinitionsList);

            ProductsController controller =new ProductsController(_productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage= await controller.GetProductsAsync();

            Assert.AreEqual(HttpStatusCode.OK,responseMessage.StatusCode);

            string expected=JsonConvert.SerializeObject(new{Count= _productDefinitionsList.Count, Products= _productDefinitionsList });
            Assert.AreEqual(expected,await responseMessage.Content.ReadAsStringAsync());
        }
        [TestMethod]
        public async Task GetProductsById_GetProduct_CorrectProductId()
        {
            string productId = "223456789123456789123456";
            ProductDefinition productDefinition =
                _productDefinitionsList.FirstOrDefault(definition => definition.Id.Equals(productId));
            _productRepositoryMock.Setup(repository => repository.GetByIdAsync(productId)).ReturnsAsync(productDefinition);

            ProductsController controller = new ProductsController(_productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.GetProductByIdAsync(productId);

            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode);

            string expected = JsonConvert.SerializeObject(productDefinition);
            Assert.AreEqual(expected, await responseMessage.Content.ReadAsStringAsync());
        }

        [TestMethod]
        public async Task GetProductsById_BadRequest_IncorrectProductId()
        {
            string productId = "22345678912345678912345";

            ProductsController controller = new ProductsController(_productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.GetProductByIdAsync(productId);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }
        [TestMethod]
        public async Task GetProductsById_NotFound_NonexistentProductId()
        {
            string productId = "923456789123456789123456";

            ProductsController controller = new ProductsController(_productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.GetProductByIdAsync(productId);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }
    }
}