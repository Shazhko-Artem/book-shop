﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using book_shop.api.Controllers;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Commands;
using Castle.Components.DictionaryAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace book_shop.api.Tests.Controllers
{
    [TestClass]
    public class OrdersControllerTest
    {
        private Moq.Mock<IRepository<ProductDefinition>> _productRepositoryMock;
        private Moq.Mock<IRepository<Order>> _orderRepositoryMock;
        private List<ProductDefinition> _productDefinitionsList;
        [TestInitialize]
        public void InitTest()
        {
            _orderRepositoryMock=new Mock<IRepository<Order>>();
            _productRepositoryMock = new Mock<IRepository<ProductDefinition>>();
            _productDefinitionsList = new List<ProductDefinition>()
            {
                new ProductDefinition(){Name = "ASP.NET MVC5", Id = "123456789123456789123456", Category = "Books",Quantity = 5,Price = 1023},
                new ProductDefinition(){Name = "ASP.NET MVC4", Id = "223456789123456789123456", Category = "Books",Quantity = 1,Price = 1013},
                new ProductDefinition(){Name = "ASP.NET MVC3", Id = "323456789123456789123456", Category = "Books",Quantity = 2,Price = 1000}
            };
        }
        [TestMethod]
        public async Task GetOrdersAsync_GetAllOrders()
        {
            List<Order> expectedOrders=new EditableList<Order>()
            {
                new Order()
                {
                    SoldProducts = new List<SoldProduct>()
                    {
                        new SoldProduct()
                            {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Quantity = 5}
                    },
                    ShoppingDetails  = new ShoppingDetails("Artem", "Shazhko", "Kharkov", "0996836415"),
                    Id = "223456789123456789123456"
                }
        };
            _orderRepositoryMock.Setup(repository => repository.GetAllAsync()).ReturnsAsync(() => expectedOrders);

            OrdersController controller = new OrdersController(_orderRepositoryMock.Object, _productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.GetOrdersAsync();

            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode);

            string expected = JsonConvert.SerializeObject(new{Count=expectedOrders.Count, Orders=expectedOrders});
            Assert.AreEqual(expected, await responseMessage.Content.ReadAsStringAsync());
        }
        [TestMethod]
        public async Task AddOrderAsync_BuyMoreThanInStock_ErrorMessage()
        {
            ShoppingDetails shopDetails = new ShoppingDetails("Artem", "Shazhko", "Kharkov", "0996836415");
            SoldProduct soldProduct = new SoldProduct()
                {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Quantity = 5};
            Order order = new Order()
            {
                SoldProducts = new List<SoldProduct>(){soldProduct},
                ShoppingDetails = shopDetails
            };
            List<ProductDefinitionMessage> expectedErrors = new List<ProductDefinitionMessage>() { new ProductDefinitionMessage(soldProduct.Id, soldProduct.Name, "No product in the specified quantity.") };

            _orderRepositoryMock.Setup(repository => repository.AddAsync(order)).ReturnsAsync(() =>
            {
                throw new InvalidOperationException(Newtonsoft.Json.JsonConvert.SerializeObject(expectedErrors));
            });

            OrdersController controller = new OrdersController(_orderRepositoryMock.Object, _productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.AddOrderAsync(order);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);

            string expected = JsonConvert.SerializeObject(new { Message = Newtonsoft.Json.JsonConvert.SerializeObject(expectedErrors) });
            Assert.AreEqual(expected, await responseMessage.Content.ReadAsStringAsync());
        }
        [TestMethod]
        public async Task AddOrderAsync_AddNewOrder()
        {
            ShoppingDetails shopDetails = new ShoppingDetails("Artem", "Shazhko", "Kharkov", "0996836415");
            Order order = new Order()
            {
                SoldProducts = new List<SoldProduct>()
                {
                    new SoldProduct()
                        {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Quantity = 5}
                },
                ShoppingDetails = shopDetails
            };

            _orderRepositoryMock.Setup(repository => repository.AddAsync(order)).ReturnsAsync(() =>
            {
                order.Id = "223456789123456789123456";
                return order;
            });

            OrdersController controller = new OrdersController(_orderRepositoryMock.Object, _productRepositoryMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());

            HttpResponseMessage responseMessage = await controller.AddOrderAsync(order);


            Assert.AreEqual(HttpStatusCode.Created, responseMessage.StatusCode);

            string expected = JsonConvert.SerializeObject(order);
            Assert.AreEqual(expected, await responseMessage.Content.ReadAsStringAsync());
        }
    }
}
