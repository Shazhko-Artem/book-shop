﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Commands;
using book_shop.api.Models.Databases.Mongodb.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace book_shop.api.Tests.Models.Cart
{
    [TestClass]
    public class CashierTest
    {
        private Mock<IRepository<Order>> _ordersRepositoryMock;
        private Mock<IRepository<ProductDefinition>> _productsRepositoryMock;
        private ShoppingDetails _shopDetails;
        private Order _order;

        [TestInitialize]
        public void InitTest()
        {
            _ordersRepositoryMock=new Mock<IRepository<Order>>();
            _productsRepositoryMock=new Mock<IRepository<ProductDefinition>>();
            _shopDetails = new ShoppingDetails("Artem", "Shazhko", "Kharkov", "0996836415");
            _order = new Order()
            {
                SoldProducts = new List<SoldProduct>()
                {
                    new SoldProduct()
                        {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Quantity = 5}
                },
                ShoppingDetails = _shopDetails,
                Id = "123456789123456789123456"
            };
        }

        [TestMethod]
        public async Task CheckoutAsync_RunDecrementProductQuantityCommandAndSaveToOrderRep()
        {
            Cashier cashbox = new Cashier(_ordersRepositoryMock.Object, _productsRepositoryMock.Object);

            book_shop.api.Models.Databases.Commands.DecrementProductQuantityCommandVisitor visitor = new DecrementProductQuantityCommandVisitor(_order.SoldProducts);

            Order actualOrder = await cashbox.CheckoutAsync(_order);

            _productsRepositoryMock.Verify(repository => repository.AcceptAsync(visitor), Times.Once);
            _ordersRepositoryMock.Verify(repository => repository.AddAsync(_order), Times.Once);
        }
    }
}
