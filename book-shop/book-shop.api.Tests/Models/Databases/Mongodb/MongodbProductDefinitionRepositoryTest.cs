﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Mongodb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace book_shop.api.Tests.Models.Databases.Mongodb
{
    [TestClass()]
    public class MongodbProductDefinitionRepositoryTests
    {
        private List<ProductDefinition> _productDefinitionsList;
        private MongodbProductDefinitionRepository _definitionRepository;

        [TestInitialize]
        public async Task InitTest()
        {
            _productDefinitionsList = new List<ProductDefinition>()
            {
                new ProductDefinition()
                    {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Category = "Books", Quantity = 5},
                new ProductDefinition()
                    {Name = "ASP.NET MVC4", Id = "223456789123456789123456", Category = "Books", Quantity = 1},
                new ProductDefinition()
                    {Name = "ASP.NET MVC3", Id = "323456789123456789123456", Category = "Books", Quantity = 2}
            };
            var context = new MongodbContext();
            await context.ProductDefinition.DeleteManyAsync(FilterDefinition<ProductDefinition>.Empty);
            await context.ProductDefinition.InsertManyAsync(_productDefinitionsList);

            _definitionRepository = new MongodbProductDefinitionRepository(context);

        }

        [TestMethod()]
        public async Task GetAllAsync_GetAllProductsFromDatabase()
        {
            IList<ProductDefinition> actualProductsDefinitions = await _definitionRepository.GetAllAsync();
            
            CollectionAssert.AreEqual(_productDefinitionsList, actualProductsDefinitions.ToList());
        }

        [TestMethod()]
        public async Task GetByIdAsync_GetProductByIDFromDatabase()
        {
            string expectedProductId = "323456789123456789123456";
            ProductDefinition expectedProductsDefinitions = _productDefinitionsList.FirstOrDefault(definition => expectedProductId.Equals(definition.Id));

            ProductDefinition actualProductDefinition = await _definitionRepository.GetByIdAsync(expectedProductId);

            Assert.AreEqual(expectedProductsDefinitions, actualProductDefinition);
        }
    }
}