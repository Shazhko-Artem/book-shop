﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Commands;
using book_shop.api.Models.Databases.Mongodb;
using book_shop.api.Models.Databases.Mongodb.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace book_shop.api.Tests.Models.Databases.Mongodb.Commands
{
    [TestClass]
    public class DecrementProductQuantityCommandTest
    {
        private MongodbProductDefinitionRepository _definitionRepository;
        private List<ProductDefinition> _productDefinitionsList;
        private ProductDefinition _productDefForSale;
        private SoldProduct _soldProduct;
        private DecrementProductQuantityCommand _command;

        [TestInitialize]
        public async Task InitTest()
        {
            _productDefForSale = new ProductDefinition()
                {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Category = "Books", Quantity = 5, Price = 1023 };

            _productDefinitionsList = new List<ProductDefinition>()
            {
               new ProductDefinition()
                    {Name = "ASP.NET MVC4", Id = "223456789123456789123456", Category = "Books", Quantity = 1,Price = 1013},
                new ProductDefinition()
                    {Name = "ASP.NET MVC3", Id = "323456789123456789123456", Category = "Books", Quantity = 2,Price = 1000},
                _productDefForSale
            };
            var context = new MongodbContext();
            await context.ProductDefinition.DeleteManyAsync(FilterDefinition<ProductDefinition>.Empty);
            await context.ProductDefinition.InsertManyAsync(_productDefinitionsList);

            _definitionRepository = new MongodbProductDefinitionRepository(context);

            _soldProduct=new SoldProduct(_productDefForSale.Id, _productDefForSale.Name, 10);
            List<SoldProduct> soldProducts = new List<SoldProduct>()
            {
                _soldProduct
            };
            _command = new DecrementProductQuantityCommand(soldProducts, _definitionRepository);

        }
        [TestMethod]
        public async Task Execute_DecrementProductQuantityFrom5_To2()
        {
            _soldProduct.Quantity = 3;

            await _command.ExecuteAsync();

            ProductDefinition actualProduct = await _definitionRepository.GetByIdAsync(_productDefForSale.Id);

            Assert.IsNotNull(actualProduct);
            Assert.AreEqual(actualProduct.Quantity, 2);
        }
        [TestMethod]
        public async Task Execute_DecrementProductQuantityToNegativeNumber_Exception()
        {
            _soldProduct.Quantity = 10;

            string actualMessage = string.Empty;
            try
            {
                await _command.ExecuteAsync();
                ProductDefinition actualProduct = await _definitionRepository.GetByIdAsync(_productDefForSale.Id);
            }
            catch (Exception e)
            {
                actualMessage = e.Message;
            }
            string expectMessage = "[{\"ProductId\":\"123456789123456789123456\",\"Name\":\"ASP.NET MVC5\",\"Message\":\"No product in the specified quantity.\"}]";

            Assert.AreEqual(expectMessage, actualMessage);
        }
    }
}
