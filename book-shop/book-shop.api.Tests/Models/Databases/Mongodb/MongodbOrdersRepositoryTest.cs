﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.api.Models.Cart;
using book_shop.api.Models.Databases;
using book_shop.api.Models.Databases.Mongodb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;

namespace book_shop.api.Tests.Models.Databases.Mongodb
{
    [TestClass]
    public  class MongodbOrdersRepositoryTest
    {
        private List<Order> _ordersList;
        private ShoppingDetails _shopDetails;
        private MongodbContext _context;
        private MongodbOrderRepository _orderRepository;

        [TestInitialize]
        public async Task TestInit()
        {
            _shopDetails=new ShoppingDetails("Artem","Shazhko","Kharkov","0996836415");
            _ordersList = new List<Order>()
            {
                new Order()
                {
                    SoldProducts = new List<SoldProduct>()
                    {
                        new SoldProduct()
                            {Name = "ASP.NET MVC5", Id = "123456789123456789123456", Quantity = 5,Price = 1023}
                    }, ShoppingDetails = _shopDetails, Id = "123456789123456789123456"
                },
                new Order()
                {
                    SoldProducts = new List<SoldProduct>()
                    {
                        new SoldProduct()
                            {Name = "ASP.NET MVC4", Id = "223456789123456789123456", Quantity = 1,Price = 1013},
                    }, ShoppingDetails = _shopDetails, Id = "223456789123456789123456"
                },
                new Order()
                {
                    SoldProducts = new List<SoldProduct>()
                    {
                        new SoldProduct()
                            {Name = "ASP.NET MVC3", Id = "323456789123456789123456", Quantity = 2,Price = 1000}
                    }, ShoppingDetails = _shopDetails, Id = "323456789123456789123456"
                }
            };
            _context = new MongodbContext();
            await _context.Orders.DeleteManyAsync(new BsonDocument());
            await _context.Orders.InsertManyAsync(_ordersList);

            _orderRepository =new MongodbOrderRepository(_context);
        }

        [TestMethod()]
        public async Task AddAsync_AddOrderToDatabaseAndReturnObject()
        {
            MongodbProductDefinitionRepository definitionRepository = new MongodbProductDefinitionRepository(_context);
            IList<ProductDefinition> productsDefinitions = await definitionRepository.GetAllAsync();

            List<SoldProduct> expectedSoldProducts = new List<SoldProduct>();
            foreach (var productDefinition in productsDefinitions)
            {
                expectedSoldProducts.Add(new SoldProduct(productDefinition.Id, productDefinition.Name, productDefinition.Quantity));
            }
            ShoppingDetails expectedShoppingDetails = _shopDetails;

            Order saleHistory = new Order(expectedSoldProducts, expectedShoppingDetails);

            Order actualHistory = await _orderRepository.AddAsync(saleHistory);
            Assert.IsNotNull(actualHistory);
            Assert.IsNotNull(actualHistory.Id);
        }

        [TestMethod()]
        public async Task GetAllAsync_GetAllSaleHistoriesFromDatabase()
        {
            MongodbOrderRepository ordersRepository = new MongodbOrderRepository(_context);

            IList<Order> actualOrders= await ordersRepository.GetAllAsync();

            Assert.IsNotNull(actualOrders);
            Assert.IsTrue(actualOrders.Count > 0);
        }
    }
}
