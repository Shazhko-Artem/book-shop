﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.WebUI.Models;
using book_shop.WebUI.Models.Databases;
using book_shop.WebUI.Models.Products;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace book_shop.WebUI.Tests.Models.Databases
{
    [TestClass]
    public class ProductRepositoryTest
    {
        [TestMethod]
        public async Task GetAllAsync_GetAllProducts()
        {
            ProductRepository repository = new ProductRepository();
            IList<Product> products = await repository.GetAllAsync();

            Assert.IsNotNull(products);
        }
        [TestMethod]
        public async Task GetByIdAsync_GetProductById()
        {
            ProductRepository repository = new ProductRepository();
            IList<Product> products = await repository.GetAllAsync();
            if (products.Count == 0) throw new Exception("In the server isn't any product");

            Product expectedProduct = products[0];
            Product actualProduct = await repository.GetByIdAsync(expectedProduct.Id);
            
            Assert.IsNotNull(actualProduct);
            Assert.AreEqual(expectedProduct,actualProduct);
        }
    }
}
