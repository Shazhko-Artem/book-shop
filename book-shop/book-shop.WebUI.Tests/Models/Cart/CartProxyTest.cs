﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.WebUI.Models.Cart;
using book_shop.WebUI.Models.Products;
using book_shop.WebUI.Models.States;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace book_shop.WebUI.Tests.Models.Cart
{
    [TestClass]
    public class CartProxyTest
    {
        private Moq.Mock<ISession> _sessionStub;

        [TestInitialize]
        public void InitTest()
        {
            _sessionStub=new Mock<ISession>();
        }
        [TestMethod]
        public void Add_Save_5_Products()
        {
            _sessionStub.Setup(session => session.Load()).Returns(() => null);
            book_shop.WebUI.Models.Cart.CartProxy cartProxy = new WebUI.Models.Cart.CartProxy(_sessionStub.Object);
            cartProxy.Load();
            Product product = new Product() { Id = "123456789123456789", Category = "book", Quantity = 10, Name = "Asp.net", Price = 500 };
            int quantity = 5;

            cartProxy.Add(product, quantity);

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            cart.Add(product, quantity);
            string expectedJson = JsonConvert.SerializeObject(cart);

            _sessionStub.Verify(session => session.Save(expectedJson), Times.Once);
        }
        [TestMethod]
        public void Remove_RemoveOneProduct()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 10, 500);
            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","book",5,500),
                soldProduct
            };
            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();

            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            _sessionStub.Setup(session => session.Load()).Returns(() => JsonConvert.SerializeObject(cart));
            book_shop.WebUI.Models.Cart.CartProxy cartProxy = new WebUI.Models.Cart.CartProxy(_sessionStub.Object);
            cartProxy.Load();

            Product product = new Product() { Id = soldProduct.Id, Category = "book", Quantity = 5, Name = "Asp.net", Price = 500 };
            cartProxy.Remove(product);
            cart.Remove(product);

            string expectedJson = JsonConvert.SerializeObject(cart);

            _sessionStub.Verify(session => session.Save(expectedJson), Times.Once);

            SoldProduct[] expectedSoldProducts = (from sp in soldProducts where sp.Id != soldProduct.Id select sp).ToArray();
            CollectionAssert.AreEqual(expectedSoldProducts, cartProxy.SoldProducts.ToArray());
        }

        [TestMethod]
        public void SetQuantity_Set10Quantity()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);
            SoldProduct[] expectedSoldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","book",5,500),
                soldProduct
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in expectedSoldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            _sessionStub.Setup(session => session.Load()).Returns(() => JsonConvert.SerializeObject(cart));
            book_shop.WebUI.Models.Cart.CartProxy cartProxy = new WebUI.Models.Cart.CartProxy(_sessionStub.Object);
            cartProxy.Load();


            int quantity = 10;
            cartProxy.SetQuantity(new Product() { Id = soldProduct.Id }, quantity);
            cart.SetQuantity(new Product() { Id = soldProduct.Id }, quantity);
            soldProduct.Quantity = quantity;

            string expectedJson = JsonConvert.SerializeObject(cart);

            _sessionStub.Verify(session => session.Save(expectedJson), Times.Once);
            CollectionAssert.AreEqual(expectedSoldProducts, cartProxy.SoldProducts.ToArray());
        }

        [TestMethod]
        public void ComputeTotalValue_SumPriceAllProducts_5000()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);
            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","asp.net",2,1000),
                new SoldProduct("223456789123456789", "asp.net", 3, 1000)
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            _sessionStub.Setup(session => session.Load()).Returns(() => JsonConvert.SerializeObject(cart));
            book_shop.WebUI.Models.Cart.CartProxy cartProxy = new WebUI.Models.Cart.CartProxy(_sessionStub.Object);
            cartProxy.Load();

            decimal expectedResult = 5000;
            decimal actualResult = cartProxy.ComputeTotalValue();

            Assert.AreEqual(expectedResult, actualResult);
        }


        [TestMethod]
        public void Clear_RemoveAllProducts()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);

            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","asp.net",2,1000),
                new SoldProduct("223456789123456789", "asp.net", 3, 1000)
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            _sessionStub.Setup(session => session.Load()).Returns(() => JsonConvert.SerializeObject(cart));
            book_shop.WebUI.Models.Cart.CartProxy cartProxy = new WebUI.Models.Cart.CartProxy(_sessionStub.Object);
            cartProxy.Load();

            cartProxy.Clear();
            cart.Clear();

            string expectedJson = JsonConvert.SerializeObject(cart);

            _sessionStub.Verify(session => session.Save(expectedJson), Times.Once);
            CollectionAssert.AreEqual(new SoldProduct[0], cart.SoldProducts.ToArray());
        }
    }
}
