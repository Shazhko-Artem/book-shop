﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using book_shop.WebUI.Models.Cart;
using book_shop.WebUI.Models.Products;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace book_shop.WebUI.Tests.Models.Cart
{
    [TestClass]
    public class CartTest
    {
        [TestMethod]
        public void Add_Add_5_Products()
        {
            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            Product soldProduct = new Product() { Id = "123456789123456789", Category = "book", Quantity = 10, Name = "Asp.net", Price = 500 };
            int quantity = 5;
            cart.Add(soldProduct, quantity);

            SoldProduct[] expectedSoldProducts = new SoldProduct[]{
                new SoldProduct(soldProduct.Id,soldProduct.Name,quantity,soldProduct.Price)
            };

            CollectionAssert.AreEqual(expectedSoldProducts, cart.SoldProducts.ToArray());
        }

        [TestMethod]
        public void Remove_RemoveOneProduct()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 10, 500);

            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","book",5,500),
                soldProduct
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();

            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product(){Id = item.Id,Name = item.Name, Quantity = 100,Price = item.Price},
                    item.Quantity);
            }
            Product product = new Product() { Id = soldProduct.Id, Category = "book", Quantity = 5, Name = "Asp.net", Price = 500 };
            cart.Remove(product);

            SoldProduct[] expectedSoldProducts = (from sp in soldProducts where sp.Id!=soldProduct.Id select sp).ToArray();
            CollectionAssert.AreEqual(expectedSoldProducts, cart.SoldProducts.ToArray());
        }

        [TestMethod]
        public void SetQuantity_Set10Quantity()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);

            SoldProduct[] expectedSoldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","book",5,500),
                soldProduct
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in expectedSoldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            int quantity = 10;
            cart.SetQuantity(new Product(){Id = soldProduct.Id}, quantity);
            soldProduct.Quantity = quantity;

            CollectionAssert.AreEqual(expectedSoldProducts, cart.SoldProducts.ToArray());
        }

        [TestMethod]
        public void ComputeTotalValue_SumPriceAllProducts_5000()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);

            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","asp.net",2,1000),
                new SoldProduct("223456789123456789", "asp.net", 3, 1000)
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            decimal expectedResult = 5000;
            decimal actualResult=cart.ComputeTotalValue();

            Assert.AreEqual(expectedResult,actualResult);
        }

        [TestMethod]
        public void Clear_RemoveAllProducts()
        {
            SoldProduct soldProduct = new SoldProduct("223456789123456789", "book", 20, 500);

            SoldProduct[] soldProducts = new SoldProduct[]{
                new SoldProduct("123456789123456789","asp.net",2,1000),
                new SoldProduct("223456789123456789", "asp.net", 3, 1000)
            };

            book_shop.WebUI.Models.Cart.Cart cart = new WebUI.Models.Cart.Cart();
            foreach (var item in soldProducts)
            {
                cart.Add(
                    new Product() { Id = item.Id, Name = item.Name, Quantity = 100, Price = item.Price },
                    item.Quantity);
            }

            cart.Clear();

            CollectionAssert.AreEqual(new SoldProduct[0], cart.SoldProducts.ToArray());
        }
    }
}
