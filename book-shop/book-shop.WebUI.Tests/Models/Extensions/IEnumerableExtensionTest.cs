﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using book_shop.WebUI.Models.Extensions;

namespace book_shop.WebUI.Tests.Models.Extensions
{
    [TestClass]
    public class IEnumerableExtensionTest
    {
        [TestMethod]
        public void FindIndex_GetItemIndex_2()
        {
            string[] array = new[] { "text 1", "text 2", "text3" };
            string text = "Hello";
            int expectedResult = 2;

            array[expectedResult] = text;

            int actualResult = array.FindIndex(s => s == text);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void IndexOf_GetItemIndex_2()
        {
            string[] array = new[] { "text 1", "text 2", "text3" };
            string text = "Hello";
            int expectedResult = 2;

            array[expectedResult] = text;

            int actualResult = array.IndexOf(text);

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
