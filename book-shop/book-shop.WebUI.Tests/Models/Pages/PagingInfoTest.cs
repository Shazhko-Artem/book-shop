﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using book_shop.WebUI.Models.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace book_shop.WebUI.Tests.Models.Pages
{
    [TestClass]
    public class PagingInfoTest
    {
        [TestMethod]
        public void TotalPages_GetQuantityPages_6()
        {
            PagingInfo info=new PagingInfo(){ItemsPerPage = 10,TotalItems = 53};

            int expectedResult = 6;
            int actualResult = info.TotalPages;

            Assert.AreEqual(expectedResult,actualResult);
        }
    }
}
